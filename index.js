function addNumbers(one,two){
	console.log("Dispayed Sum of " + one + " and " + two );
	console.log(one+two);
}

function substractNumbers(one,two){
	console.log("Dispayed substraction of " + one + " and " + two );
	console.log(one-two);
}

addNumbers(5,15);

substractNumbers(20,5);

function multiplyNumbers(one,two){
	console.log("The product of " + one + " and " + two);
	return one*two;

}

function divideNumbers(one,two){
	console.log("The quotient of " + one + " and " + two);
	return one/two;
}

let product = multiplyNumbers(50,10);
console.log(product);

let quotient = divideNumbers(50,10);
console.log(quotient);

function area(radius){
	console.log("The result of area of circle with radius " + radius + " is:")
	return 3.142*(radius)**2;
}

let circleArea = area(15);
console.log(circleArea);

function average(one,two,three,four){
	console.log("The average of " + one +", "+ two +", "+ three +" and "+ four + ":");
	return (one+two+three+four)/4;
}
let averageVar = average(20,40,60,80);
console.log(averageVar);

function passingScore(your,total){
	console.log("Is " + your + "/" + total + " a passing score?");
	let percent = your*100/total;
	return percent >= 75;
}

let isPassingScore = passingScore(38,50);
console.log(isPassingScore);